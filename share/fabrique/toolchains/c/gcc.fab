#
# Support for the GCC (and g++) compiler.
#
# Arguments:
#   force_colour    force the compiler to colour diagnostic messages even if not on a TTY
#   lto             perform link-time optimization
#
force_colour = args.colour_diagnostics ? false;
lto = args.lto ? false;


# Find comnpiler binaries:
which = import('which');
compilers = record
{
	cc = which.executable('gcc');
	cxx = which.executable('g++');
};

# Most of this functionality is just a wrapper around generic "GCC-like" functionality.
platform = args.platform ? import('platform');
gcc = import('gcc-like.fab', platform=platform);

#
# GCC specializations:
#
compile_flags =
	(if force_colour ['-fdiagnostics-color=always'] else [])
	+
	(if lto ['-flto'] else [])
	+
	['-fpermissive' '-Wno-unknown-pragmas']
	;

linker_flags =
	(if lto ['-flto'] else [])
	;

toolchain = gcc.create_toolchain(compilers, compile_flags, linker_flags);
compile = toolchain.compile_srcs;

binary = function(objects:list[file[in]], binary:file[out],
                  options:gcc.Options, libraries:list[string]=[],
                  extra_flags:list[string]=[], cxx:bool=false, dynamic:bool=false): file
{
	flags = gcc.binary_flags(options, cxx, libraries, dynamic);
	compiler = if cxx compilers.cxx else compilers.cc;

	gcc.link_binary(compiler, objects, binary, linker_flags+flags)
};

shared_library = function(objects:list[file[in]], libname:string, subdir:file,
                          extra_flags:list[string]=[], cxx:bool=false): file[out]
{
	compiler = if cxx compilers.cxx else compilers.cc;
	gcc.library(compiler, objects, libname, subdir, shared=true, extra_flags=extra_flags)
};
