//! @file plugin/Loader.cc    Definition of @ref fabrique::plugin::Loader
/*
 * Copyright (c) 2014 Jonathan Anderson
 * All rights reserved.
 *
 * This software was developed by SRI International and the University of
 * Cambridge Computer Laboratory under DARPA/AFRL contract (FA8750-10-C-0237)
 * ("CTSRD"), as part of the DARPA CRASH research programme.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include <fabrique/Out.hh>
#include <fabrique/strings.hh>
#include <fabrique/platform/SharedLibrary.hh>
#include <fabrique/platform/files.hh>
#include <fabrique/plugin/Loader.hh>
#include <fabrique/plugin/Registry.hh>

using namespace fabrique;
using namespace fabrique::platform;
using namespace fabrique::plugin;
using std::string;
using std::vector;


Loader::Loader(const vector<string>& paths)
	: paths_(paths)
{
}


std::weak_ptr<Plugin> Loader::Load(string name)
{
	const string libname = LibraryFilename(name);

	Out& dbg = Out::Debug("plugin.loader");
	dbg
		<< Out::Action << "searching"
		<< Out::Reset << " for "
		<< Out::Type << "plugin "
		<< Out::Literal << name
		<< Out::Operator << " ("
		<< Out::Literal << libname
		<< Out::Operator << ")"
		<< Out::Reset << " in paths "
		<< Out::Operator << "["
		<< Out::Reset << " "
		<< Out::Literal << join(paths_, " ")
		<< Out::Operator << " ]"
		<< Out::Reset << "\n"
		;

	const string filename = FindFile(libname, paths_, FileIsSharedLibrary,
	                                 DefaultFilename(""));

	dbg
		<< Out::Reset << "found "
		<< Out::Operator << "'"
		<< Out::Literal << filename
		<< Out::Operator << "'"
		<< Out::Reset << "\n"
		;

	if (filename.empty())
		return {};

	libraries_.emplace_back(SharedLibrary::Load(filename));
	return Registry::get().lookup(name);
}
