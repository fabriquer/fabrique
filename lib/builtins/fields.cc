//! @file  builtins/fields.cc    Definition of fields() builtin function
/*
 * Copyright (c) 2018-2019 Jonathan Anderson
 * All rights reserved.
 *
 * This software was developed at Memorial University of Newfoundland
 * under the NSERC Discovery program (RGPIN-2015-06048).
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include <fabrique/SemanticException.hh>
#include <fabrique/builtins.hh>
#include <fabrique/dag/DAGBuilder.hh>
#include <fabrique/dag/Parameter.hh>
#include <fabrique/dag/TypeReference.hh>
#include <fabrique/types/RecordType.hh>
#include <fabrique/types/TypeContext.hh>

using namespace fabrique;
using namespace fabrique::builtins;
using namespace fabrique::dag;


/**
 * Enumerate the fields in a record.
 */
static ValuePtr FieldsImpl(ValueMap, DAGBuilder&, SourceRange);

dag::ValuePtr builtins::Fields(DAGBuilder &b)
{
	TypeContext &types = b.typeContext();
	SharedPtrVec<dag::Parameter> params;
	params.emplace_back(new Parameter("value", types.nilType()));

	return b.Function(FieldsImpl, types.recordType({}), params);
}

static ValuePtr
FieldsImpl(ValueMap arguments, DAGBuilder &b, SourceRange src)
{
	auto v = arguments["value"];
	SemaCheck(v, src, "null value");

	const auto &t = v->type();

	ValueMap fields;
	for (auto i : t.fields())
	{
		fields[i.first] = TypeReference::Create(i.second, src);
	}

	return ValuePtr(b.Record(fields, src));
}
