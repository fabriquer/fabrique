//! @file  builtins/import.cc    Definition of import() builtin function
/*
 * Copyright (c) 2018-2019 Jonathan Anderson
 * All rights reserved.
 *
 * This software was developed at Memorial University of Newfoundland
 * under the NSERC Discovery program (RGPIN-2015-06048).
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include <fabrique/AssertionFailure.hh>
#include <fabrique/Out.hh>
#include <fabrique/SemanticException.hh>
#include <fabrique/builtins.hh>
#include <fabrique/names.hh>
#include <fabrique/ast/EvalContext.hh>
#include <fabrique/dag/DAGBuilder.hh>
#include <fabrique/dag/File.hh>
#include <fabrique/dag/Parameter.hh>
#include <fabrique/parsing/Parser.hh>
#include <fabrique/platform/files.hh>
#include <fabrique/plugin/Loader.hh>
#include <fabrique/plugin/Plugin.hh>
#include <fabrique/plugin/Registry.hh>
#include <fabrique/types/FileType.hh>
#include <fabrique/types/TypeContext.hh>

#include <fstream>

using namespace fabrique;
using namespace fabrique::builtins;
using namespace fabrique::dag;
using namespace fabrique::platform;
using std::string;


struct Fabfile
{
	string subdir;
	string filename;

	operator bool() { return (not filename.empty()); }
};

static Fabfile
FindFabfile(string name, string subdir, string srcroot,
            std::list<string> modulePaths, SourceRange);

static std::shared_ptr<Record>
ImportFile(string filename, string subdir, ValueMap arguments, SourceRange src,
           parsing::Parser &p, ast::EvalContext &eval, Out &dbg);

static Fabfile
TryPath(string name, string root, string subdir, Out&, SourceRange);


ValuePtr
fabrique::builtins::Import(parsing::Parser &p, plugin::Loader &pluginLoader,
                           string srcroot, const std::list<string> &modulePaths,
                           ast::EvalContext &eval)
{
	FAB_ASSERT(PathIsAbsolute(srcroot), "srcroot must be an absolute path");

	DAGBuilder &b = eval.builder();
	TypeContext &types = b.typeContext();

	SharedPtrVec<dag::Parameter> params;
	params.emplace_back(new Parameter("module", types.stringType()));

	dag::Function::Evaluator import =
		[&p, &eval, &pluginLoader, srcroot, modulePaths]
		(dag::ValueMap arguments, dag::DAGBuilder &builder, SourceRange src)
	{
		Out &dbg = Out::Debug("module.import");

		auto n = arguments["module"];
		SemaCheck(n, src, "missing module or file name");
		arguments.erase("module");
		const string name = n->str();

		auto s = arguments[names::Subdirectory];
		SemaCheck(s, src, "missing subdir");
		arguments.erase(names::Subdirectory);

		auto currentSubdir = std::dynamic_pointer_cast<dag::File>(s);
		SemaCheck(currentSubdir, src, "subdir is not a File");
		const string subdir = currentSubdir->str();

		dbg
			<< Out::Action << "importing "
			<< Out::Operator << "'"
			<< Out::Literal << name
			<< Out::Operator << "'"
			<< Out::Reset << " from subdir '"
			<< Out::Literal << subdir
			<< Out::Reset << "'...\n"
			;

		//
		// Is the name a filename?
		//
		if (auto f = FindFabfile(name, subdir, srcroot, modulePaths, src))
		{
			return ImportFile(f.filename, f.subdir, arguments, src,
			                  p, eval, dbg);
		}

		//
		// If not, it must be a plugin name.
		//
		auto descriptor = plugin::Registry::get().lookup(name).lock();
		if (not descriptor)
		{
			descriptor = pluginLoader.Load(name).lock();
		}
		SemaCheck(descriptor, n->source(),
			"no such file or plugin ('" + name + "')");

		auto plugin = descriptor->Create(builder, arguments);
		SemaCheck(plugin, src, "failed to create plugin with arguments");

		dbg
			<< Out::Action << "instantiated "
			<< Out::Type << "plugin"
			<< Out::Operator << "'"
			<< Out::Literal << name
			<< Out::Operator << "': "
			<< Out::Reset << plugin->type()
			<< "\n"
			;

		return plugin;
	};

	return b.Function(import, types.nilType(), params,
	                  SourceRange::None(), true);
}


static Fabfile
FindFabfile(string name, string subdir, string srcroot,
            std::list<string> modulePaths, SourceRange src)
{
	Out &dbg = Out::Debug("import.lookup.file");
	dbg
		<< Out::Action << "searching for "
		<< Out::Operator << "'"
		<< Out::Literal << name
		<< Out::Operator << "'"
		<< Out::Reset << " in subdir '"
		<< Out::Literal << subdir
		<< Out::Reset << "'...\n"
		;

	if (PathIsAbsolute(name))
	{
		return TryPath(name, "", "", dbg, src);
	}

	modulePaths.push_front(srcroot);
	for (string root : modulePaths)
	{
		if (auto f = TryPath(name, subdir, root, dbg, src))
		{
			return f;
		}
	}

	return {};
}


static std::shared_ptr<Record>
ImportFile(string filename, string subdir, ValueMap arguments, SourceRange src,
           parsing::Parser &p, ast::EvalContext &eval, Out &dbg)
{
	dbg
		<< Out::Action << "importing "
		<< Out::Type << "file"
		<< Out::Operator << "'"
		<< Out::Literal << filename
		<< Out::Operator << "'"
		<< Out::Reset << " from "
		<< Out::Literal << subdir
		<< Out::Reset << "\n"
		;

	DAGBuilder &b = eval.builder();
	auto sub = b.File(subdir);
	arguments[names::Subdirectory] = sub;

	auto scope = eval.EnterScope(filename);
	scope.DefineReserved(names::Arguments, b.Record(arguments));
	scope.DefineReserved(names::BuildDirectory, sub);
	scope.Define(names::Subdirectory, sub);

	std::ifstream infile(filename.c_str());
	SemaCheck(infile, src, "failed to open '" + filename + "'");

	auto parse = p.ParseFile(infile, filename);
	for (auto &e : parse.errors())
	{
		Out::Stderr() << e << "\n";
	}

	SemaCheck(parse, src, "failed to import '" + filename + "'");

	ValueMap values;
	for (auto &v : parse.ok())
	{
		auto val = eval.Define(*v);
		if (auto &name = v->name())
		{
			values[name->name()] = val;
		}
	}

	return b.Record(values, src);
}


static Fabfile
TryPath(string name, string subdir, string root, Out &dbg, SourceRange src)
{
	const string nameWithinSubdir = JoinPath(subdir, name);
	const string fullPath = JoinPath(root, nameWithinSubdir);

	dbg
		<< Out::Action << "trying "
		<< Out::Operator << "'"
		<< Out::Literal << root
		<< Out::Operator << "'/'"
		<< Out::Literal << subdir
		<< Out::Operator << "'/'"
		<< Out::Literal << name
		<< Out::Operator << "'"
		<< Out::Reset << "'...\n"
		;

	if (PathIsFile(fullPath))
	{
		return {JoinPath(subdir, DirectoryOf(name)), fullPath};
	}
	else if (PathIsDirectory(fullPath))
	{
		const string fabFilePath = JoinPath(fullPath, "fabfile");
		SemaCheck(PathIsFile(fabFilePath), src,
			"directory '" + fullPath + "' does not contain 'fabfile'");

		return {nameWithinSubdir, fabFilePath};
	}
	else
	{
		return {};
	}
}
